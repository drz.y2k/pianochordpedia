let chortDictionary: Record<string, string[]> = {
	'1': ['C note'],
	'2': ['D note'],
	'3': ['E note'],
	'4': ['F note'],
	'5': ['G note'],
	'6': ['A note'],
	'7': ['B note'],
	'8': ['C#/Db note'],
	'9': ['D#/Eb note'],
	'10': ['F#/Gb note'],
	'11': ['G#/Ab note'],
	'12': ['A#/Bb note'],
	'1_3_5': ['C'],
	'7_14_22': ['Bm'],
	'2_7_10': ['Bm/D'],
	'7_10_14': ['Bm/F#'],
	'7_20_22': ['Bsus2'],
	'7_8_10': ['Bsus2/C#'],
	'7_10_20': ['Bsus2/F#', 'F#sus4', 'Bsus2/Gb '],
	'7_15_22': ['Bsus4'],
	'3_7_10': ['Bsus4/E'],
	'7_10_15': ['Bsus4/F#'],
	'5_7_14': ['G'],
	'7_14_17': ['G/B'],
	'2_5_7': ['G/D'],
	'3_5_7': ['Em'],
	'5_7_15': ['Em/G'],
	'7_15_17': ['Em/B'],
	'5_7_20': ['G(b5)'],
	'5_7_14_16': ['G6'],
	'3_5_7_20': ['Em6'],
	'10_12_20': ['F#'],
	'3_5_12': ['Edim'],
	'6_15_20': ['A'],
	'3_6_8': ['A/C#'],
	'3_6_20': ['A/E'],
	'2_6_10': ['D'],
	'6_10_14': ['D/F#'],
	'6_14_22': ['D/A'],
	'4_6_13': ['F'],
	'6_13_16': ['F/A'],
	'1_4_6': ['F/C'],
	'3_7_11': ['E'],
	'3_8_11': ['C#min'],
	'6_10_20': ['F#min'],
};

let chordOptions: string[] = Object.entries(chortDictionary)
	.map((el): string[] => el[1])
	.flat();

function filterChordOptions(chordQuery: string): string[] {
	return chordOptions
		.filter(
			(el) =>
				el.toLowerCase().includes(chordQuery.toLowerCase()) &&
				!el.toLowerCase().includes('note')
		)
		.slice(0, 5);
}

function findChord(chord: string) {
	const foundEntry = Object.entries(chortDictionary).find(
		(entry: [string, string[]]) => entry[1].includes(chord)
	);

	return foundEntry ? foundEntry[0].split('_').map((el) => parseInt(el)) : [];
}

export default function checkChord(chord: number[]): string[] {
	const chordSimpified = chord.join('_');
	// console.log(chordSimpified);
	return chortDictionary[chordSimpified];
}

let chordLength = chordOptions.filter(
	(el) => !el.toLowerCase().includes('note')
).length;

export { filterChordOptions, findChord, chordLength };
//1 12
//13 24
//25 36
//37 48
//49 60
//61 72
