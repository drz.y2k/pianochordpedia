import { useState, useEffect } from 'react';
import React from 'react';

interface KeyboardProps {
	firstItemIndex: number;
	onKeyboardPressed: (octave: number, arr: number[]) => void;
	octave: number;
	parentNotes: number[] | undefined;
}

interface KeyState {
	index: number;
	pressed: boolean;
}

interface KeyboardState {
	pressedKeys: KeyState[];
}

const blackNotesName = ['C#/Db', 'D#/Eb', '', 'F#/Gb', 'G#/Ab', 'A#/Bb'];
const whiteNotesName=['C','D','E','F','G','A','B'];

export const Keyboard = ({
	firstItemIndex,
	onKeyboardPressed,
	octave,
	parentNotes,
}: KeyboardProps) => {
	let indexCounter = firstItemIndex;

	const fillPressedKeys = (): KeyState[] => {
		const keys = [];
		for (let i = firstItemIndex; i < firstItemIndex + 12; i++) {
			keys.push({ index: i, pressed: false });
		}
		return keys;
	};

	const [pressedKeys, setPressedKeys] = useState<
		KeyboardState['pressedKeys']
	>(() => fillPressedKeys());

	useEffect(() => {
		if (parentNotes?.length == 0) {
			setPressedKeys(fillPressedKeys());
		} else {
			const newPressedKeys = pressedKeys.map((key) =>
				parentNotes?.includes(key.index)
					? { index: key.index, pressed: true }
					: { index: key.index, pressed: false }
			);
			setPressedKeys(newPressedKeys);
		}
	}, [parentNotes]);

	const checkPressedKey = (e: React.MouseEvent<HTMLDivElement>) => {
		const keyName = Number(e.currentTarget.getAttribute('id'));

		const newPressedKeys = pressedKeys.map((key) =>
			key.index === keyName
				? { index: key.index, pressed: !key.pressed }
				: key
		);
		setPressedKeys(newPressedKeys);
		onKeyboardPressed(
			octave,
			newPressedKeys.filter((key) => key.pressed).map((key) => key.index)
		);
	};

	const checkIndexPressedKey = (index: number) => {
		return pressedKeys.find((key) => key.index == index)?.pressed;
	};

	const createWhites = () => {
		const whites = [];
		for (let i = 0; i < 7; i++) {
			whites.push(
				<React.Fragment key={indexCounter}>
					<div
						key={indexCounter}
						className={`white ${
							checkIndexPressedKey(indexCounter) ? 'pressed' : ''
						}`}
						id={String(indexCounter)}
						onClick={checkPressedKey}
					>
						<div className="white-chord_name">{whiteNotesName[i]}</div>
					</div>
					
				</React.Fragment>
			);
			indexCounter++;
		}
		return whites;
	};

	const createBlacks = () => {
		const blacks = [];
		for (let i = 0; i < 6; i++) {
			if (i == 2) {
				blacks.push(
					<div
						className="unshow"
						key={`unshow${firstItemIndex}`}
					></div>
				);
				continue;
			}
			blacks.push(
				<React.Fragment key={indexCounter}>
					<div
						className={`black
				 ${checkIndexPressedKey(indexCounter) ? 'pressed' : ''}`}
						id={String(indexCounter)}
						onClick={checkPressedKey}
					>
						<div className="black-chord_name">
							{blackNotesName[i]}
						</div>
					</div>
				</React.Fragment>
			);
			indexCounter++;
		}
		return blacks;
	};

	return (
		<div className="cont">
			<div className="whites">{createWhites()}</div>
			<div className="blacks">{createBlacks()}</div>
		</div>
	);
};
