import { useState, useEffect } from 'react';
import './styles/main.scss';
import { Keyboard } from './components/Keyboard';
import checkChord, {
	filterChordOptions,
	findChord,
	chordLength,
} from './utils/checkChord';

function App() {
	const [chord, setChord] = useState<string[]>();
	const [octavesKeys, setOctavesKeys] = useState<{ octave: number[] }>({
		octave: [],
	});
	const keyboardOctavesCount = 3;

	const [queryChord, setQueryChord] = useState<string>('');

	let onlyKeyIndex =
		octavesKeys != null
			? Object.entries(octavesKeys)
					.map((el) => el[1])
					.flat()
			: [];

	const [keyIndex, setKeyIndex] = useState<number[]>(onlyKeyIndex);

	const handleKeyPressed = (octave: number, arr: number[]): void => {
		setOctavesKeys((prev) => ({ ...prev, [octave]: arr }));
	};

	useEffect(() => {
		setChord(checkChord(onlyKeyIndex));
		setKeyIndex(keyIndex);
	}, [octavesKeys]);

	const changeTheme = () => {
		const html = document.querySelector('html');
		if (html?.classList.contains('dark')) {
			html.classList.remove('dark');
		} else {
			html?.classList.add('dark');
		}
	};

	const handleQueryChord = (e: React.FocusEvent<HTMLInputElement>) => {
		setQueryChord(e.target.value);
	};

	const selectedQueryChord = (el: string) => {
		let onlyKeyIndex = findChord(el);
		setKeyIndex(onlyKeyIndex);
		setChord(checkChord(onlyKeyIndex));
		setQueryChord('');
	};

	const clearNotes = () => {
		setOctavesKeys({ octave: [] });
		setKeyIndex([]);
		setQueryChord('');
	};

	return (
		<div className="App min-h-screen flex flex-col justify-center items-center min-w-fit p-3 zo">
			{/* {JSON.stringify(onlyKeyIndex)} */}
			<div className="flex flex-row gap-4">
				<div className="text-4xl font-bold text-center mb-1">
					{chord?.join(', ') || 'PianoChordPedia'}
				</div>

				<div className="relative group">
					<div
						className={`bg-slate-200 rounded-md absolute z-50 shadow-2xl w-full top-11  ${
							!queryChord && 'invisible'
						}`}
					>
						<ul className="divide-y divide-slate-300 text-black rounded-md">
							{filterChordOptions(queryChord).map((el, i) => (
								<li
									key={i}
									onMouseDown={() => selectedQueryChord(el)}
									className="p-1 hover:bg-slate-300 hover:cursor-pointer rounded-md"
								>
									{el}
								</li>
							))}
						</ul>
					</div>
					<input
						value={queryChord}
						onFocus={(e) => {
							handleQueryChord(e);
							e.target.select();
						}}
						onInput={handleQueryChord}
						onBlur={() => setQueryChord('')}
						type="text"
						className="rounded-md p-2 mb-2 text-black focus:outline-none shadow-2xl"
						placeholder="Search some chord..."
					/>
					<button
						className="rounded-md bg-slate-500 p-2 ml-2 active:bg-slate-600"
						onClick={clearNotes}
					>
						CLEAR
					</button>
				</div>
			</div>

			<div className="text-sm mb-10">
				There are currently {chordLength} chords stored
			</div>

			<div className="flex flex-row justify-center items-center h-full">
				{new Array(keyboardOctavesCount).fill('').map((el, i) => (
					<Keyboard
						key={i}
						firstItemIndex={i * 12 + 1}
						onKeyboardPressed={handleKeyPressed}
						octave={el}
						parentNotes={keyIndex}
					/>
				))}
			</div>
		</div>
	);
}

export default App;
